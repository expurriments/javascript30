# Day 1 - JavaScript Drum Kit

## Today I learned

* [`data-*` attributes](https://developer.mozilla.org/en-US/docs/Learn/HTML/Howto/Use_data_attributes) attributes on HTML elements
    > The `data-*` global attributes form a class of attributes called custom data attributes, that allow proprietary information to be exchanged between the HTML and its DOM representation by scripts ([MDN on `data-*`](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/data-*))
    * They are a great solution for custom element-associated metadata
    * Do not store content that should be visible and accessible in data attributes, because assistive technology may not access them
* Add event listener for `keydown` event type with [`addEventListener`](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener)
* Find elements that match the specified group of (CSS) selectors with [`querySelectorAll`](https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelectorAll)
* Add and remove class from an element using the read-only [`element.classList](https://developer.mozilla.org/en-US/docs/Web/API/Element/classList) property with its methods `add()` and `remove()`.
    > Using `classList` is a convenient alternative to accessing an element's list of classes as a space-delimited string via `element.className`.

## Things I should have learned before, but I learned today

### [HTML `kbd` element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/kbd)

The HTML `<kbd>` element represents user input and produces an inline element displayed in the browser's default monospace font.

An example from today's code:

```html
<div data-key="65" class="key">
    <kbd>A</kbd>
    <span class="sound">clap</span>
</div>
```

## Things I should know by now, but I don't

* [`flex` CSS property](https://developer.mozilla.org/en-US/docs/Web/CSS/flex)
    * [ ] [A complete guide to Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
* Font size. `rem`?
    * [ ] [CSS Font-Size: em vs. px vs. pt vs. percent](https://kyleschaeffer.com/development/css-font-size-em-vs-px-vs-pt-vs/)
    * [ ] [Comprehensive Guide: When to Use Em vs. Rem](https://webdesign.tutsplus.com/tutorials/comprehensive-guide-when-to-use-em-vs-rem--cms-23984)