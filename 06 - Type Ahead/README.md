# Day 6 - Ajax Type Ahead


## Things I have done differently

* After parsing the population string to number, I used [`toLocaleString`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toLocaleString) with the `'en-US'` locale for displaying large numbers with commas. In my opinion, it is less hacky and more explicit.
* Use capturing group to keep case while highlighting match
    > [Solution from Stack Overflow](https://stackoverflow.com/a/19161971/)

## Things I should know by now, but I still don't

* Regex and capturing groups