# Day 8 - Fin with HTML5 Canvas


## Things I have done differently

* I went all nuts with weird ES6 and cryptic code. I'm not really recommending it, but I did it for the lulz.

## Today I learned

* [`globalCompositeOperation`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/globalCompositeOperation)  sets the type of compositing operation to apply when drawing new shapes, where type is a string identifying which of the compositing or blending mode operations to use.

## Things I should have learned before, but I learned today

## Things I should know by now, but I still don't
