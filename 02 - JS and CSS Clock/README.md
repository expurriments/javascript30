# Day 2 - JS and CSS Clock

## Things I have done differently

* Refactored rotation of hands into a handy little function: `rotateHand(element, current, max)`. d534ddf4afe7ea329d75544cd5de63687caa70fe
* Invoke `setDate` immediately with IIFE and `setDate` calls itself recursively with `setTimeout` delay, so there is no huge jump of hands. Using `setInterval`, the first invocation of `setDate` happens with a 1000 ms delay, which is noticeable and results in bad user experience. 0b9fcfe2352a965b9ef4d7011364289489e47d60
* Make the different hands styled differently: second hand is red and small, hour hand is short, but thick. c5404efe84b7d250d806d208459bb9abc06a19e4
* Remove transition temporarily at 90 degrees. cb55a7b7ed07ef1c4e3d433fbd92c4b6a9f5c630
