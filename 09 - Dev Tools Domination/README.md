# Day 9 - Dev Tools Domination

When you don't know what is causing changes to the DOM elements, you can set up a break on attribute modifications, for example when some JavaScript code changes the color of a paragraph to green.

### `console`

* `console.log` strings can be interpolated, though it's not that relevant since [ES6 template literals](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals) (backtick syntax). For more info, see [format specifiers](https://developers.google.com/web/tools/chrome-devtools/console/console-write)
* with `console.assert` you can test for something, and if it is false, an error message will be displayed in the console.
* `console.time` and `console.timeEnd` lets you measure time
* `console.count` helps you counting the occurences of console calls?
* `console.group` and `console.groupEnd` will let you group messages

Open today's `index.html` for examples
