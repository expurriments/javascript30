# Day 11 - Custom Video Player

## Things I have done differently

* added `input` event listeners to the ranges to continuously update the volume and playback rate (not just when we finish moving the sliders)
* used `progress` event instead of `timeupdate` (afaik, there's not much difference, and I liked the `progress` event name more)
* used one-liner, short functions

## Today I learned

### CSS Magic

The CSS is also interesting, see [`style.css`](./style.css)

### Methods, properties and events on different `HTMLElement`s.

#### [`video`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/video)

* properties
  * `currentTime`
  * `duration`
  * `paused`
  * `volume`
  * `playbackRate`
* methods
  * `pause()`
  * `play()`
* events
  * `play`
  * `pause`
  * `progress`
  * `timeupdate`

#### [Range input `input[type="range"]`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/range)

* properties
  * `value`
  * `name`
* [`change` event gets triggered only if we drop the slider to a new position](https://stackoverflow.com/questions/18544890/onchange-event-on-input-type-range-is-not-triggering-in-firefox-while-dragging), therefore we added `input` event listener to trigger the `onchange` listeners every time we change the values of the slider

### Mousedown and change

```js
let mousedown = false;
progress.addEventListener('click', scrub);
progress.addEventListener('mousemove', (e) => mousedown && scrub(e));
progress.addEventListener('mousedown', () => mousedown = true);
progress.addEventListener('mouseup', () => mousedown = false);
```
