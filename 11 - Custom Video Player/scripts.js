/** DOM */
// Get the "papa" div
const player = document.querySelector('.player');
// Get the video
const video = player.querySelector('.viewer');
// Get the controls
const progress = player.querySelector('.progress');
const progressBar = player.querySelector('.progress__filled');
const toggle = player.querySelector('.toggle');
const skipButtons = player.querySelectorAll('[data-skip]');
const ranges = player.querySelectorAll('.player__slider');

/** Handlers */
function togglePlay() {
  video[video.paused ? 'play' : 'pause']();
}

function updateButton() {
  toggle.textContent = this.paused ? '►' : '❚ ❚';
}

function skip() {
  video.currentTime += parseFloat(this.dataset.skip);
}

function updateRange() {
  video[this.name] = this.value;
}

function handleProgress() {
  progressBar.style.flexBasis = `${video.currentTime / video.duration * 100}%`;
}

function scrub(e) {
  video.currentTime = e.offsetX / progress.offsetWidth * video.duration;
}

/** Event listeners */
// You can toggle the video by clicking on the video or the toggle button
video.addEventListener('click', togglePlay);
toggle.addEventListener('click', togglePlay);
// Make sure the play/pause button is in sync
// Better add the listener to the play event than
// hooking directly into the togglePlay function
// because the video could be started even without
// using the togglePlay function
video.addEventListener('play', updateButton);
video.addEventListener('pause', updateButton);
// Skip based on data attribute values
skipButtons.forEach(button => button.addEventListener('click', skip));
// Range sliders
// They have a name "volume" and "playbackRate"
// Change only triggers when the change ends
ranges.forEach(range => range.addEventListener('change', updateRange));
// We want to update the range while we are moving the range slider
ranges.forEach(range => range.addEventListener('input', updateRange));

// Progress bar
video.addEventListener('progress', handleProgress);
let mousedown = false;
progress.addEventListener('click', scrub);
progress.addEventListener('mousemove', (e) => mousedown && scrub(e));
progress.addEventListener('mousedown', () => mousedown = true);
progress.addEventListener('mouseup', () => mousedown = false);
