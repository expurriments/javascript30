# Day 3 - CSS Variables


## Things I have done differently

* Use `'input'` event listeners instead of `'mousemove'`. It makes updates faster, won't trigger so many event listeners, and the color picker updates continuously on change. 3655388f2485c69c7a456e3a474bcf12a39f3d42

## Today I learned

* CSS variables can be updated with JavaScript. When you update a variable from JavaScript in CSS, everywhere on the page where that variable is referenced will update itself. With Sass, variables are defined at compile/transpile time and then you cannot change it. CSS variables have their scope, too.
