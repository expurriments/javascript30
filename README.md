# JavaScript 30

My code repo of [Wes Bos'](http://wesbos.com/) free course on JS, JavaScript 30, available at [`javascript30.com`](https://javascript30.com/)

The original starter files are available at [`github.com/wesbos/JavaScript30`](https://github.com/wesbos/JavaScript30).

## Things I learned, should learn and should have known

After completing the daily coding challenges, I make notes on what the main topic of the daily video was. I write down what I learned that day. Sometimes, the thing I learned was not even mentioned by the author (usually because it's CSS and HTML related). Sometimes, I don't have enough time to research a topic, so I leave a note to myself with some links that look relevant to the question at hand.

Links to authoritative sources are also included, usually starting with the [MDN Web Docs](https://developer.mozilla.org).

> [Avatar Image](https://unsplash.com/photos/Y_pLBbSAhHI) by [`unsplash.com/@mewmewmew`](https://unsplash.com/@mewmewmew)