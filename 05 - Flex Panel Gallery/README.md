# Day 5 - Flex Panel Gallery

* [What the FLEXBOX? `flexbox.io`](https://flexbox.io) - free course Wes Bos.

## Things I have done differently

* created simple closure for toggling a class on an element. Providing `condition` function to the closure makes toggling dependent on the result of `condition(event)`'s return value.